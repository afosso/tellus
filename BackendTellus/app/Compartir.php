<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Compartir extends Model
{
    use SoftDeletes;
    protected $table = "compartir";
    protected $primarykey = "idCompartir";
    protected $fillable = ['idNoticia', 'idUsuario'];
}
