<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departamento extends Model
{
    use SoftDeletes;
    protected $table = "departamento";
    protected $primarykey = "idDepartamento";
    protected $fillable = ['idPais', 'codigoDANE', 'nombre'];
}
