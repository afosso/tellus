<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Etiqueta extends Model
{
    use SoftDeletes;
    protected $table = "etiqueta";
    protected $primarykey = "idEtiqueta";
    protected $fillable = ['nombreEtiqueta'];
}
