<?php

namespace App\Http\Controllers;

use App\Compartir;
use Illuminate\Http\Request;

class CompartirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compartir  $compartir
     * @return \Illuminate\Http\Response
     */
    public function show(Compartir $compartir)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compartir  $compartir
     * @return \Illuminate\Http\Response
     */
    public function edit(Compartir $compartir)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compartir  $compartir
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compartir $compartir)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compartir  $compartir
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compartir $compartir)
    {
        //
    }
}
