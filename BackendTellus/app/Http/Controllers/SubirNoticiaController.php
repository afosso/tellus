<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class SubirNoticiaController extends Controller
{
    public function uploadFile(Request $request){

        $file = $request->file('file');
        $nombre = $file->getClientOriginalName();
        Storage::disk('local')->put($nombre, File::get($file));
        $urlVideo = public_path() . "/storage/" . $nombre;
        DB::insert('INSERT INTO noticia(idUsuario, tituloVideo, descripcion, fechaPublicacion, urlVideo) VALUES(?,?,?,(SELECT CURRENT_TIMESTAMP),?)', [$request->idUsuario, $request->titulo, $request->descripcion, $urlVideo]);
    }
}
