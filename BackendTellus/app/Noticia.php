<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noticia extends Model
{
    use SoftDeletes;
    protected $table = "noticia";
    protected $primarykey = "idNoticia";
    protected $fillable = ['idUsuario', 'tituloVideo', 'descripcion', 'fechaPublicacion', 'urlVideo'];
}
