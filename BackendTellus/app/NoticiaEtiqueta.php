<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoticiaEtiqueta extends Model
{
    use SoftDeletes;
    protected $table = "noticiaetiqueta";
    protected $primarykey = "idNoticiaEtiqueta";
    protected $fillable = ['idNoticia', 'idEtiqueta'];
}
