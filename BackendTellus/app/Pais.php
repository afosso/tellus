<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pais extends Model
{
    use SoftDeletes;
    protected $table = "pais";
    protected $primarykey = "idPais";
    protected $fillable = ['codigoDANE', 'nombre'];
}
