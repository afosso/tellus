<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reaccion extends Model
{
    use SoftDeletes;
    protected $table = "reaccion";
    protected $primarykey = "idReaccion";
    protected $fillable = ['idUsuario', 'idNoticia', 'tipoReaccion'];
}
