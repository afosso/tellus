<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona', function (Blueprint $table) {
            $table->bigIncrements('idPersona');
            $table->unsignedBigInteger('idUsuario')->unique();
            $table->foreign('idUsuario')->references('idUsuario')->on('usuario');
            $table->unsignedBigInteger('idDepartamento')->nullable();
            $table->foreign('idDepartamento')->references('idDepartamento')->on('departamento');
            $table->string('nombre', 100);
            $table->string('apellido', 100);
            $table->enum('genero', ['F', 'M']);
            $table->date('fechaNacimiento');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona');
    }
}
