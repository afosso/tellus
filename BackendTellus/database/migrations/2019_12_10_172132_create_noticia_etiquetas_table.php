<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiaEtiquetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticiaetiqueta', function (Blueprint $table) {
            $table->bigIncrements('idNoticiaEtiqueta');
            $table->unsignedBigInteger('idNoticia');
            $table->unsignedBigInteger('idUsuario');
            $table->foreign('idNoticia')->references('idNoticia')->on('noticia');
            $table->foreign('idUsuario')->references('idUsuario')->on('usuario');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticiaetiqueta');
    }
}
