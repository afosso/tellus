<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReaccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reaccion', function (Blueprint $table) {
            $table->bigIncrements('idReaccion');
            $table->unsignedBigInteger('idNoticia');
            $table->unsignedBigInteger('idUsuario');
            $table->enum('tipoReaccion', ['Like', 'No Like']);
            $table->foreign('idNoticia')->references('idNoticia')->on('noticia');
            $table->foreign('idUsuario')->references('idUsuario')->on('usuario');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reaccion');
    }
}
