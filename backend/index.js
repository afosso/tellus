const express = require('express');
const asyncify = require('express-asyncify');
const fileUpload = require('express-fileupload')
const http = require('http');
const bodyParser = require('body-parser');
const usuario = require('./model/usuario-model');
const persona = require('./model/persona-model');
const subirVideo = require('./model/subir-video');


const port = 4003;
const app = asyncify(express());
const server = http.createServer(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());

/*Rutas de usuario*/

app.post("/Persona/NewPerson", persona.registerNewPeople);
app.get("/Usuario/:usuario", usuario.iniciarSesion);
app.post("/SubirVideo", subirVideo.subirVideo);
app.get("/Noticias", subirVideo.consultarNoticias);


server.listen(port, () => {
    console.log(`server listening on port ${port}`);
});