let conexion = require('./bd');

let getAllPerson = (request, response) => {
    conexion.connection.query("SELECT * FROM persona", (error, result) => {
        if (error) throw new Error(error);
        response.status(200).json(result[0]);
    });
}

let getPersonaById = (request, response) => {
    let id = request.params.id;
    conexion.connection.query("SELECT * FROM persona WHERE idPersona = ?", [id], (error, result) => {
        if (error) throw new Error(error);
        response.status(200).json(result[0]);
    });
};

let registerNewPeople = (request, response) => {
    let { nombres, apellidos, correo, contrasena, genero, fechaNacimiento } = request.body;
    let sql = "CALL SaveNewUser(?, ?, ?, ?, ?, ?);";
    conexion.connection.query(sql, [nombres, apellidos, correo, contrasena, genero, fechaNacimiento], (error, result) => {
        if (error) response.status(500).json(error);
        response.status(200).json(true);
    });

}

module.exports = { getPersonaById, getAllPerson, registerNewPeople };