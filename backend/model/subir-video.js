const conexion = require('./bd');

let subirVideo = (request, response) => {
    let file = request.files.file;
    let idUsuario = request.body.idUsuario;
    let titulo = request.body.titulo;
    let descripcion = request.body.descripcion;
    let nombreVideo = file.name;

    // Subimos vídeo
    file.mv(`./files/${file.name}`, err => {
        if (err) return response.status(200).json(false);
    });

    // Insertamos registro
    conexion.connection.query("INSERT INTO noticia(idUsuario, tituloVideo, descripcion, fechaPublicacion, urlVideo) VALUES(?,?,?,(SELECT CURRENT_TIMESTAMP),?)", [idUsuario, titulo, descripcion, nombreVideo], (error, result) => {
        if (error) return response.status(200).json(false);
        return response.status(200).json(true);
    });
}

let consultarNoticias = (request, response) => {
    conexion.connection.query("SELECT n.*, p.nombre, p.apellido FROM noticia n INNER JOIN persona p ON p.idUsuario = n.idUsuario", (error, result) => {
        if (error) throw new Error(error);
        response.status(200).json(result);
    });
}

module.exports = { subirVideo, consultarNoticias };