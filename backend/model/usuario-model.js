let conexion = require('./bd.js');

let getUsuarioById = (request, response) => {
    let id = request.params.idUsuario;
    conexion.connection.query('SELECT * FROM usuario WHERE idUsuario = ?', id, (error, results, fields) => {
        if (error) throw new Error(error);
        console.log(results);
        response.status(200).json(results[0]);
    });
};

let iniciarSesion = (request, response) => {
    let correo = request.params.usuario;
    conexion.connection.query("SELECT * FROM usuario WHERE email = ?", correo, (error, result) => {
        if (error) response.status(200).json("Error");
        response.status(200).json(result[0]);
    });
}

let getAllUsuario = (req, response) => {
    conexion.connection.query("SELECT * FROM usuario", (error, results, fields) => {
        if (error) throw new Error(error);
        response.status(200).json(results[0]);
    });
}

let saveUsuario = (request, response) => {
    console.log(request.body);
    let { email, password, estado } = request.body;

    conexion.connection.query('INSERT INTO usuario(email, password, estado) VALUES (?,?,?)', [email, password, estado], (error, results) => {
        if (error) response.status(500).json(false);
        response.status(200).json(true);
    });
}

let updateUsuario = (request, response) => {
    let { idUsuario, email, password, estado } = request.body;
    conexion.connection.query(
        'UPDATE usuario SET email = ?, password = ?, estado = ? WHERE idUsuario = ?', [email, password, estado, idUsuario],
        (error, results) => {
            if (error) response.status(500).json(false);
            response.status(200).json(true);
        });
}

let deleteUsuario = (request, response) => {
    let idUsuario = request.params.idUsuario;
    conexion.connection.query("UPDATE usuario SET estado = 0 WHERE idUsuario = ?", [idUsuario], (error) => {
        if (error) response.status(500).json(false);
        response.status(200).json(true);
    })
}

module.exports = { getUsuarioById, iniciarSesion, getAllUsuario, saveUsuario, updateUsuario, deleteUsuario };