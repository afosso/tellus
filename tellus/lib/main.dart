import 'package:flutter/material.dart';
import 'package:tellus/src/blocs/provider.dart';
import 'package:tellus/src/routes/routes.dart';
import 'package:tellus/src/utils/preferences_user.dart';

void main() async {
  final prefs = new PreferenciasUsuario(); 
  await prefs.initPrefs();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Tellus",
        theme: ThemeData(
          primaryColor: Color.fromRGBO(30, 128, 129, 1.0),
          backgroundColor: Color.fromRGBO(30, 128, 129, 1.0),
          buttonColor: Color.fromRGBO(30, 128, 129, 1.0),
          bottomAppBarColor: Color.fromRGBO(146, 190, 181, 1.0),
          fontFamily: 'Roboto'
        ),
        initialRoute: prefs.sessionIniciada ? '/' : 'login',
        routes: getApplicationRoutes()),
    );
  }
}
