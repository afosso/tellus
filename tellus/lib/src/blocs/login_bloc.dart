import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:tellus/src/blocs/validators.dart';

class LoginBloc with Validators {

  final _emailController            = BehaviorSubject<String>();
  final _passwordController         = BehaviorSubject<String>();
  final _nombresController          = BehaviorSubject<String>();
  final _apellidosController        = BehaviorSubject<String>();
  final _passwordConfirmController  = BehaviorSubject<String>();
  final _terminosController         = BehaviorSubject<bool>();
  final _generoController           = BehaviorSubject<String>();
  final _fechaNacimientoController  = BehaviorSubject<String>();
  
  // recuperar los datos del stream
  Stream<String> get emailStream            => _emailController.stream.transform(validarEmail);
  Stream<String> get passwordStream         => _passwordController.stream.transform(validarPassword);
  Stream<String> get nombresStream          => _nombresController.stream.transform(validarNombre);
  Stream<String> get apellidoStream         => _apellidosController.stream.transform(validarApellidos);
  Stream<String> get passwordConfirmStream  => _passwordConfirmController.stream.transform(validarPassword);
  Stream<bool>   get terminosStream         => _terminosController.stream.transform(validarTerminos);
  Stream<String> get generoStream           => _generoController.stream.transform(validarGenero);
  Stream<String> get fechaNacimientoStream  => _fechaNacimientoController.stream.transform(validarFechaNacimiento);

  Stream<bool> get formValidStream =>
    Observable.combineLatest2(emailStream, passwordStream, (e,p) => true);
  
  Stream<bool> get formValidRegisterStream =>
    Observable.combineLatest8(
                  nombresStream, 
                  apellidoStream, 
                  emailStream, 
                  passwordStream, 
                  passwordConfirmStream,
                  terminosStream,
                  generoStream,
                  fechaNacimientoStream,
                  (n,a,e,p,pc,t, g, fn) => true);

  // Insertar valores al stream
  Function(String) get changeEmail    => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;
  Function(String) get changeNombre   => _nombresController.sink.add;
  Function(String) get changeApellido => _apellidosController.sink.add;
  Function(String) get changePasswordConfirm => _passwordConfirmController.sink.add;
  Function(bool)   get changeTerminos => _terminosController.sink.add;
  Function(String) get changeGenero => _generoController.sink.add;
  Function(String) get changeFechaNacimiento => _fechaNacimientoController.sink.add;

  // obtener el ultimo valor ingresado a los streams
  String get email      => _emailController.value;
  String get password   => _passwordController.value;
  String get nombres    => _nombresController.value;
  String get apellidos  => _apellidosController.value;
  String get passwordConfirm => _passwordConfirmController.value;
  bool   get terminos   => _terminosController.value;
  String get genero     => _generoController.value;
  String get fechaNacimiento => _fechaNacimientoController.value;

  dispose(){
    _emailController?.close();
    _passwordController?.close();
    _nombresController?.close();
    _apellidosController?.close();
    _passwordConfirmController?.close();
    _terminosController?.close();
    _generoController?.close();
    _fechaNacimientoController?.close();
  }
}