

import 'dart:async';

class Validators {

  final validarPassword = StreamTransformer<String, String>.fromHandlers(
    handleData: (password, sink) {
      if(password.length >= 6){
        sink.add(password);
      }else{
        sink.addError('La contraseña debe tener mas de 6 carácteres');
      }
    }
  );

  final validarEmail = StreamTransformer<String, String>.fromHandlers(
    handleData: (email, sink) {
      Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

      RegExp regExp = new RegExp(pattern);
      if( regExp.hasMatch(email) ){
        sink.add(email);
      } else{
        sink.addError('Debe ser un correo valido.');
      }
    }
  );

  final validarNombre = StreamTransformer<String, String>.fromHandlers(
    handleData: (nombre, sink) {
      if(nombre.length >= 3){
        sink.add(nombre);
      }else{
        sink.addError('El nombre debe contener mas de 3 caracteres');
      }
    }
  );

  final validarApellidos = StreamTransformer<String, String>.fromHandlers(
    handleData: (apellido, sink) {
      if(apellido.length >= 3){
        sink.add(apellido);
      }else{
        sink.addError('Los apellidos deben contener mas de 3 caracteres');
      }
    }
  );

  final validarTerminos = StreamTransformer<bool, bool>.fromHandlers(
    handleData: (terminos, sink){
      if(terminos){
        sink.add(terminos);
      }else{
        sink.addError('Debe seleccionar los términos y condiciones');
      }
    }
  );

  final validarGenero = StreamTransformer<String, String>.fromHandlers(
    handleData: (genero, sink){
      if(genero.length >= 1){
        sink.add(genero);
      }else{
        sink.addError("Debe seleccionar un género");
      }
    }
  );

  final validarFechaNacimiento = StreamTransformer<String, String>.fromHandlers(
    handleData: (fechaNacimiento, sink){
      if(fechaNacimiento.length > 0){
        sink.add(fechaNacimiento);
      }else{
        sink.addError("Debe seleccionar una fecha de nacimiento");
      }
    }
  );

}