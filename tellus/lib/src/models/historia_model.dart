// To parse this JSON data, do
//
//     final historia = historiaFromJson(jsonString);

import 'dart:convert';

Historia historiaFromJson(String str) => Historia.fromJson(json.decode(str));

String historiaToJson(Historia data) => json.encode(data.toJson());

class Historia {
    String id;
    String nombre;
    String ruta;

    Historia({
        this.id,
        this.nombre,
        this.ruta,
    });

    factory Historia.fromJson(Map<String, dynamic> json) => new Historia(
        id: json["id"],
        nombre: json["nombre"],
        ruta: json["ruta"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "ruta": ruta,
    };
}
