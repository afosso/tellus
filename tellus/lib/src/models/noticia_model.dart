// To parse this JSON data, do
//
//     final noticia = noticiaFromJson(jsonString);

import 'dart:convert';

Noticia noticiaFromJson(String str) => Noticia.fromJson(json.decode(str));

String noticiaToJson(Noticia data) => json.encode(data.toJson());

class Noticia {
    int idNoticia;
    int idUsuario;
    String tituloVideo;
    String descripcion;
    String fechaPublicacion;
    String urlVideo;
    String nombres;
    String apellidos;

    Noticia({
        this.idNoticia,
        this.idUsuario,
        this.tituloVideo,
        this.descripcion,
        this.fechaPublicacion,
        this.urlVideo,
        this.nombres,
        this.apellidos
    });

    factory Noticia.fromJson(Map<String, dynamic> json) => Noticia(
        idNoticia: json["idNoticia"],
        idUsuario: json["idUsuario"],
        tituloVideo: json["tituloVideo"],
        descripcion: json["descripcion"],
        fechaPublicacion: json["fechaPublicacion"],
        urlVideo: json["urlVideo"],
        nombres: json["nombre"],
        apellidos: json["apellido"]
    );

    Map<String, dynamic> toJson() => {
        "idNoticia": idNoticia,
        "idUsuario": idUsuario,
        "tituloVideo": tituloVideo,
        "descripcion": descripcion,
        "fechaPublicacion": fechaPublicacion,
        "urlVideo": urlVideo,
        "nombre" : nombres,
        "apellido" : apellidos
    };
}
