import 'package:tellus/src/models/historia_model.dart';

class HistoriaProvider{

  Future<List<Historia>> getHistorias() async {
    List<Historia> _listHistoria = new List<Historia>();

    _listHistoria.add(new Historia(id : "1", nombre: "Carretera de la tranquilidad", ruta: "assets/card1.jpg"));
    _listHistoria.add(new Historia(id : "2", nombre: "Puente de arco", ruta: "assets/card2.jpg"));
    _listHistoria.add(new Historia(id : "3", nombre: "Lago Tranquilizante", ruta: "assets/card3.jpg"));
    _listHistoria.add(new Historia(id : "4", nombre: "Ruta del tren", ruta: "assets/card4.jpg"));
    return _listHistoria;
  }

}