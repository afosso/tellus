import 'package:tellus/src/models/noticia_model.dart';
import 'package:http/http.dart' as http;
import 'package:tellus/src/utils/url_backend.dart';
import 'dart:convert';

class NoticiaProvider{

  Future<List<Noticia>> getNoticias() async {
    final response = await http.get(returnMethodServer('Noticias'));
    final decodedData = json.decode(response.body);
    final List<Noticia> noticias = new List();
    if(decodedData == null ) return [];
    for (var item in decodedData) {
      final prodTemp = Noticia.fromJson(item);
      noticias.add(prodTemp);
    }
    return noticias;
  }

}