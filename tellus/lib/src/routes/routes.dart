import 'package:flutter/material.dart';
import 'package:tellus/src/screens/camera.dart';
import 'package:tellus/src/screens/home.dart';
import 'package:tellus/src/screens/security/register.dart';
import 'package:tellus/src/screens/security/startsession.dart';
import 'package:tellus/src/screens/video_config.dart';

Map<String, WidgetBuilder> getApplicationRoutes(){
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => Home(),
    'register': (context) => Register(),
    'login': (context) => StartSession(),
    'camera' : (context) => CameraApp(),
    'config_video' : (context) => VideoConfigPage()
  };
}
