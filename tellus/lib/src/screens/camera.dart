import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tellus/src/utils/preferences_user.dart';
import 'package:tellus/src/utils/utils.dart';

List<CameraDescription> cameras;

class CameraApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Subir nueva noticia'),
      ),
      body: SafeArea(child: CameraExampleHome()),
    );
  }
}

class CameraExampleHome extends StatefulWidget {

  @override
  _CameraExampleHomeState createState(){
    return _CameraExampleHomeState();
  }
}

class _CameraExampleHomeState extends State<CameraExampleHome> with WidgetsBindingObserver {
  CameraController controller;
  String videoPath;
  bool delante = true;
  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  @override
  void initState() {
    getCameras();
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  Future<void> getCameras() async {
    cameras = await availableCameras();
    for(CameraDescription cameraDescription in cameras) {
      if(cameraDescription.lensDirection == CameraLensDirection.back){
        onNewCameraSelected(cameraDescription);
        return;
      }
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        onNewCameraSelected(controller.description);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Center(
              child: _cameraPreviewWidget(),
          ),
          decoration: BoxDecoration(
            color: Colors.black,
            border: Border.all(
              color: controller != null && controller.value.isRecordingVideo
                  ? Colors.redAccent
                  : Colors.grey,
              width: 3.0,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 20.0),
          width: double.infinity,
          height: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _captureControlRowWidget()
            ],
          ),
        ),

        Container(
          margin: EdgeInsets.only(bottom: 30.0, left: 15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              _cameraTogglesRowWidget(),
            ],
          ),
        )
      ],
    );
  }

  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Tap a camera',
        style: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.w900,
        ),
      );
    } else {
      return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CameraPreview(controller),
      );
    }
  }

  Widget _captureControlRowWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        GestureDetector(
          child: CircleAvatar(
            backgroundColor: Colors.red,
            child: controller != null &&
                  controller.value.isInitialized &&
                  !controller.value.isRecordingVideo
                ? Icon(Icons.videocam, color: Colors.white)
                : Icon(Icons.stop, color: Colors.white),
            radius: 30.0,
          ),
          onTap: controller != null &&
                  controller.value.isInitialized &&
                  !controller.value.isRecordingVideo
              ? onVideoRecordButtonPressed
              : onStopButtonPressed,
        ),
      ],
    );
  }

  Widget _cameraTogglesRowWidget() {
    return GestureDetector(
      child: CircleAvatar(
        child: delante 
            ? Icon(Icons.camera_rear, color: Colors.white, size: 20.0)
            : Icon(Icons.camera_front, color: Colors.white, size: 20.0),
        radius: 20.0,
        backgroundColor: Colors.grey,
      ),
      onTap: (){
        if(controller == null || !controller.value.isRecordingVideo){
          if(delante){
            delante = false;
            for(CameraDescription cameraDescription in cameras) {
              if(cameraDescription.lensDirection == CameraLensDirection.back){
                onNewCameraSelected(cameraDescription);
                return;
              }
            }
          }else{
            delante = true;
            for(CameraDescription cameraDescription in cameras) {
              if(cameraDescription.lensDirection == CameraLensDirection.front){
                onNewCameraSelected(cameraDescription);
                return;
              }
            }
          }
        }
      },
    );
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    // if (controller != null) {
    //   await controller?.dispose();
    // }
    controller = new CameraController(
      cameraDescription,
      ResolutionPreset.medium,
      enableAudio: true,
    );


    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        retornarSnackBar(context, 'Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller?.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void onVideoRecordButtonPressed() {
    startVideoRecording().then((String filePath) {
      if (mounted) setState(() {});
      if (filePath != null) retornarSnackBar(context, 'Saving video to $filePath');
    });
  }

  void onStopButtonPressed() {
    stopVideoRecording().then((_) {
      if (mounted) setState(() {});
      retornarSnackBar(context, 'Video recorded to: $videoPath');
    });
    Navigator.pushReplacementNamed(context, 'config_video', arguments: videoPath);
  }

  Future<String> startVideoRecording() async {
    if (!controller.value.isInitialized) {
      retornarSnackBar(context, 'Error: select a camera first.');
      return null;
    }

    final _prefs = new PreferenciasUsuario();
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Movies/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}_${_prefs.idUsuario.toString()}.mp4';

    if (controller.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return null;
    }

    try {
      videoPath = filePath;
      await controller.startVideoRecording(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  Future<void> stopVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.stopVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }

  }

  void _showCameraException(CameraException e) {
    retornarSnackBar(context, 'Error: ${e.code}\n${e.description}');
  }

}