import 'package:flutter/material.dart';

class Events extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          _cardInfo("Super Rebajon Olimpica", "since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book", "assets/event1.jpg"),
          _cardInfo("Super Rebajon Olimpica", "since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book", "assets/event1.jpg"),
          _cardInfo("Super Rebajon Olimpica", "since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book", "assets/event1.jpg"),
        ],
      )
    );
  }

  Widget _cardInfo(String title, String description, String assetImage){
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      elevation: 10,
      margin: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(5),
            child: Row(
              children: <Widget>[
                Text(
                  title,
                  textAlign: TextAlign.start,
                  style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(5),
            child: Text(description, textAlign: TextAlign.start),
          ),
          Container(
            height: 200,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(assetImage),
                    fit: BoxFit.contain
                )
            ),
          ),
          Container(
            margin: EdgeInsets.all(5),
            child: Row(
              children: <Widget>[
                Text('Etiquetas: '),
                FlatButton(
                  onPressed: (){},
                  child: Text('Promoción', style: TextStyle(color: Color(0xFF1E8081)),),
                ),
                FlatButton(
                  onPressed: (){},
                  child: Text('Olimpica SAO', style: TextStyle(color: Color(0xFF1E8081)),),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

