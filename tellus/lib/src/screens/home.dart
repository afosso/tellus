import 'package:flutter/material.dart';

import 'events.dart';
import 'menu.dart';
import 'videos.dart';

class Home extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: _tabBar(context),
            title: CircleAvatar(
              backgroundImage: AssetImage('assets/ic_launcher.png'),
              backgroundColor: Colors.white,
            ),
            actions: <Widget>[
              _acciones()
            ],
          ),
          body: _cuerpo(),
          floatingActionButton: _botonCamara(context),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        )
    );
  }

  Widget _acciones(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      child: CircleAvatar(
        child: Icon(Icons.search, color: Colors.white,),
        backgroundColor: Colors.white12,
      ),
    );
  }

  Widget _tabBar(BuildContext context){
    return TabBar(
      tabs: [Tab(icon: Icon(Icons.home)),Tab(icon: Icon(Icons.calendar_today)),Tab(icon: Icon(Icons.menu))],
      indicatorColor: Theme.of(context).primaryColor,
    );
  }

  Widget _cuerpo(){
    return TabBarView(
      children: [Videos(),Events(),Menu()],
    );
  }

  Widget _botonCamara(BuildContext context) {
    return FloatingActionButton(
      onPressed: () => Navigator.pushNamed(context, 'camera'),
      child: Icon(Icons.camera),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }

  

}