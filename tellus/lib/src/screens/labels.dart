import 'package:flutter/material.dart';

class Labels extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Etiquetas'),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text('Promoción', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text('Rebajas', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text('Deportes', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text('Naturaleza', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text('Ciclismo', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text('Motociclismo', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text('Cultural', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text('Diversidad', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text('Tecnología', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),
          ListTile(
            title: Text('Tercera Guerra Mundial', style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Divider(),

        ],
      ),
    );
  }

}