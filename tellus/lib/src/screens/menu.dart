import 'package:flutter/material.dart';
import 'regions.dart';
import 'labels.dart';

class Menu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountEmail: Text('afosso@misena.edu.co'),
            accountName: Text('Andrés Felipe Osso Cortés'),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage('http://i.pravatar.cc/300'),
            ),
          ),
          Card(
            child: InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => Labels()));
              },
              child: ListTile(
                  leading: Icon(Icons.label),
                  title: Text('Etiquetas')
              ),
            )
          ),
          Card(
            child: InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => Regions()));
              },
              child: ListTile(
                  leading: Icon(Icons.map),
                  title: Text('Regiones')
              ),
            )
          ),
          Card(
            child: ListTile(
                leading: Icon(Icons.video_library),
                title: Text('Videos guardados')
            ),
          ),
          Card(
            child: ListTile(
                leading: Icon(Icons.star),
                title: Text('Favoritos')
            ),
          ),
          Card(
            child: ListTile(
                leading: Icon(Icons.delete),
                title: Text('Eliminar cuenta')
            ),
          )
        ],
      ),
    );
  }
}