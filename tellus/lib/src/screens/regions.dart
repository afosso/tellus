import 'package:flutter/material.dart';

class Regions extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Regiones'),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text('Regional Huila', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),
          ListTile(
            title: Text('Regional Amazonas', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),
          ListTile(
            title: Text('Regional Antioquia', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),
          ListTile(
            title: Text('Regional Atlantico', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),
          ListTile(
            title: Text('Regional Bolivar', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),
          ListTile(
            title: Text('Regional Boyacá', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),
          ListTile(
            title: Text('Regional Caldas', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),
          ListTile(
            title: Text('Regional Caquetá', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),
          ListTile(
            title: Text('Regional Cundinamarca', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),
          ListTile(
            title: Text('Regional Santander', style: TextStyle(fontWeight: FontWeight.bold)),
            trailing: FlatButton.icon(onPressed: (){}, icon: Icon(Icons.arrow_forward_ios), label: Text('')),
          ),
          Divider(),

        ],
      ),
    );
  }

}