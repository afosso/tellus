import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tellus/src/blocs/provider.dart';
import 'package:tellus/src/utils/terminosycondiciones.dart';
import 'package:http/http.dart' as http;
import 'package:tellus/src/utils/url_backend.dart';

class Register extends StatefulWidget {
  Register({Key key}) : super(key: key);

  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController _fechaNacimientoController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    final fondo = Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/fondo_tellus.jpg'),
          fit: BoxFit.cover
        )
      ),
    );

    return Scaffold(
      body: Stack(
        children: <Widget>[
          fondo,
          _loginBox(context)
        ],
      ),
    );
  }

  Widget _loginBox(BuildContext context){
    final logo = Column(
      children: <Widget>[
        CircleAvatar(
          radius: 70.0,
          child: Image(
            image: AssetImage('assets/logo_blanco.png'),
            fit: BoxFit.contain,
          ),
          backgroundColor: Color.fromRGBO(30, 128, 129, 0.0),
        ),
      ],
    );

    final bloc = Provider.of(context);

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding: EdgeInsets.symmetric(vertical: 20.0),
            child: Column(
              children: <Widget>[
                logo,
                Text('Bienvenido', style: TextStyle(fontSize: 25.0, color: Colors.white, fontWeight: FontWeight.bold)),
                SizedBox(height: 20.0),
                _inputNombres(bloc),
                SizedBox(height: 20.0),
                _inputApellidos(bloc),
                SizedBox(height: 20.0),
                _inputCorreo(bloc),
                SizedBox(height: 20.0),
                _inputContrasena(bloc),
                SizedBox(height: 20.0),
                _inputConfirmContrasena(bloc),
                SizedBox(height: 20.0),
                _inputGenero(bloc),
                SizedBox(height: 20.0),
                _crearFecha(context, bloc),
                SizedBox(height: 20.0),
                _terminosCondiciones(bloc),
                SizedBox(height: 20.0),
                _botonIngreso(bloc),
                
              ],
            ),
          ),
          //_botonRegistro(context),
          SizedBox(height: 100.0)
        ],
      ),
    );
  }

  Widget _inputNombres(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.nombresStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: TextField(
            cursorColor: Colors.white,
            textCapitalization: TextCapitalization.words,
            style: TextStyle(fontFamily: DefaultTextStyle.of(context).style.fontFamily, color: Colors.white),
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),  
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)), 
              labelText: 'Nombres',
              labelStyle: TextStyle(color: Colors.white, fontFamily: DefaultTextStyle.of(context).style.fontFamily),
              errorText: snapshot.error
            ),
            onChanged: bloc.changeNombre
          ),
        );
      },
    );
  }

  Widget _inputApellidos(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.apellidoStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: TextField(
            textCapitalization: TextCapitalization.words,
            style: TextStyle(fontFamily: DefaultTextStyle.of(context).style.fontFamily, color: Colors.white),
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),  
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)), 
              labelText: 'Apellidos',
              labelStyle: TextStyle(color: Colors.white, fontFamily: DefaultTextStyle.of(context).style.fontFamily),
              errorText: snapshot.error
            ),
            onChanged: bloc.changeApellido
          ),
        );
      },
    );
  }

  Widget _inputCorreo(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.emailStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: TextField(
            cursorColor: Colors.white,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),  
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)), 
              labelText: 'Correo Electrónico',
              labelStyle: TextStyle(color: Colors.white, fontFamily: DefaultTextStyle.of(context).style.fontFamily),
              errorText: snapshot.error
            ),
            style: TextStyle(fontFamily: DefaultTextStyle.of(context).style.fontFamily, color: Colors.white),
            onChanged: bloc.changeEmail
          ),
        );
      },
    );
  }

  Widget _inputContrasena(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: TextField(
            cursorColor: Colors.white,
            textCapitalization: TextCapitalization.words,
            obscureText: true,
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),  
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)), 
              labelText: 'Contraseña',
              labelStyle: TextStyle(color: Colors.white, fontFamily: DefaultTextStyle.of(context).style.fontFamily),
              errorText: snapshot.error
            ),
            style: TextStyle(fontFamily: DefaultTextStyle.of(context).style.fontFamily, color: Colors.white),
            onChanged: bloc.changePassword
          ),
        );
      },
    );
  }

  Widget _inputConfirmContrasena(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.passwordConfirmStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: TextField(
            cursorColor: Colors.white,
            textCapitalization: TextCapitalization.words,
            obscureText: true,
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),  
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)), 
              labelText: 'Confirmar Contraseña',
              labelStyle: TextStyle(color: Colors.white, fontFamily: DefaultTextStyle.of(context).style.fontFamily),
              errorText: snapshot.error
            ),
            style: TextStyle(fontFamily: DefaultTextStyle.of(context).style.fontFamily, color: Colors.white),
            onChanged: bloc.changePasswordConfirm
          ),
        );
      },
    );
  }

  Widget _inputGenero(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.generoStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: DropdownButton<String>(
            value: bloc.genero,
            icon: Icon(Icons.arrow_downward, color: Colors.white,),
            iconSize: 24,
            hint: Text('Seleccione un género'),
            elevation: 16,
            isExpanded: true,
            style: TextStyle( 
              color: Colors.black
            ),
            underline: Container(
              height: 2,
              color: Colors.white,
            ),
            onChanged: bloc.changeGenero,
            items: <String>['Masculino', 'Femenino'].map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value.substring(0,1),
                child: Container(
                  color: Colors.transparent,
                  child: Text(value),
                ),
              );
            }).toList(),

          )
        );
      },
    );
  }

  Widget _terminosCondiciones(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.terminosStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return GestureDetector(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text('Términos y condiciones', style: TextStyle(color: Colors.white)),
                Switch(
                  value: bloc.terminos ?? false,
                  onChanged: (value) {
                    _mostrarAlertaTerminos(context, bloc);
                  },
                  activeColor: Colors.white,
                ),
              ],
            )
          ),
          onTap: () => _mostrarAlertaTerminos(context, bloc),
        );
      },
    );
  }

  Widget _botonIngreso(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.formValidRegisterStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
            child: Text('Ingresar'),            
          ),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          elevation: 10.0,
          color: Color.fromRGBO(30, 128, 129, 1.0),
          textColor: Colors.white,
          onPressed: snapshot.hasData ? () => _register(context, bloc) : null,
        );
      },
    );
  }

  _register(BuildContext context, LoginBloc bloc) async {
    if(bloc.password != bloc.passwordConfirm){
      _mostrarAlertaPasswords(context);
      return;
    }
    
    if(!bloc.terminos){
      _mostrarAlertaTerminos(context, bloc);
      return;
    }

    // DE Aqui para abajo se realiza el envío a la API para guardar los datos,
    // por favor si está funcionando no deben tocarlo por nada del mundo...!
    final respuesta = await http.post( returnMethodServer("RegistrarPersona"), body: {
      "nombres": bloc.nombres,
      "apellidos" : bloc.apellidos,
      "correo" : bloc.email,
      "contrasena" : bloc.password,
      "genero" : bloc.genero,
      "fechaNacimiento" : bloc.fechaNacimiento
    });

    if(respuesta.statusCode != 200){
      final respuestaJson = json.decode(respuesta.body);
      if(respuestaJson["errno"] == 1062){
        _mostrarAlertaBackend(context, "Ya existe un registro con el correo ingresado.", respuesta.statusCode);
      }else{
        _mostrarAlertaBackend(context, respuestaJson["sqlMessage"], respuesta.statusCode);
      }
      
    }else{
      _mostrarAlertaBackend(context, "Registro exitoso, puede iniciar sesión.", respuesta.statusCode);
    }
  }

  Widget _crearFecha(BuildContext context, LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.fechaNacimientoStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: TextFormField(
            controller: _fechaNacimientoController,
            enableInteractiveSelection: false,
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),  
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)), 
              labelText: 'Fecha de Nacimiento',
              labelStyle: TextStyle(color: Colors.white, fontFamily: DefaultTextStyle.of(context).style.fontFamily),
              errorText: snapshot.error
            ),
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
              _selectDate(context, bloc);
            },
            style: TextStyle(fontFamily: DefaultTextStyle.of(context).style.fontFamily, color: Colors.white),
            onChanged: bloc.changeFechaNacimiento,
          ),
        );
      },
    );
  }

  void _selectDate(BuildContext context, LoginBloc bloc) async {
    DateTime picked = await showDatePicker(
      context: context,
      firstDate: new DateTime(1950),
      initialDate: new DateTime(DateTime.now().year - 18, DateTime.now().month, DateTime.now().day),
      lastDate: new DateTime(DateTime.now().year - 18, DateTime.now().month, DateTime.now().day),
    );

    if (picked != null) {
      setState(() {
        DateTime _fecha = picked;
        String dateSlug ="${_fecha.year.toString()}-${_fecha.month.toString().padLeft(2,'0')}-${_fecha.day.toString().padLeft(2,'0')}";
        bloc.changeFechaNacimiento(dateSlug);
        _fechaNacimientoController.text = bloc.fechaNacimiento;
      });
    }
  }

  _mostrarAlertaPasswords(context){
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          content: Container(
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.error, color: Colors.red),
                Text('Las contraseñas no coinciden.')
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: (){
                Navigator.pop(context);
              },
              child: Text('Aceptar', style: TextStyle(color: Theme.of(context).primaryColor),),
            )
          ],
        );
      }
    );
  }

  _mostrarAlertaTerminos(context, LoginBloc bloc){

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          content: obtenerTerminosyCondiciones(context, DefaultTextStyle.of(context).style.fontFamily),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar', style: TextStyle(color: Theme.of(context).primaryColor)),
              onPressed: () { 
                Navigator.pop(context);
                bloc.changeTerminos(false);
              },
            ),
            FlatButton(
              child: Text('Aceptar', style: TextStyle(color: Theme.of(context).primaryColor)),
              onPressed: () { 
                Navigator.pop(context);
                bloc.changeTerminos(true);
              },
            )
          ],
          elevation: 20.0,
        );
      }
    );
  }

  _mostrarAlertaBackend(context, String mensaje, int code){
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          content: Container(
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(mensaje)
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: (){
                if(code == 200){
                Navigator.pushNamed(context, 'login');
                }else{
                  Navigator.pop(context);
                }
              },
              child: Text('Aceptar', style: TextStyle(color: Theme.of(context).primaryColor),),
            )
          ],
        );
      }
    );
  }
}