
import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:tellus/src/blocs/provider.dart';
import 'package:http/http.dart' as http;
import 'package:tellus/src/utils/preferences_user.dart';
import 'package:tellus/src/utils/url_backend.dart';
import 'package:tellus/src/utils/utils.dart';

class StartSession extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final fondo = Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/fondo_tellus.jpg'),
          fit: BoxFit.cover
        )
      ),
    );

    return Scaffold(
      body: Stack(
        children: <Widget>[
          fondo,
          _loginBox(context)
        ],
      ),
    );
  }

  Widget _loginBox(BuildContext context){
    final logo = Column(
      children: <Widget>[
        CircleAvatar(
          radius: 70.0,
          child: Image(
            image: AssetImage('assets/logo_blanco.png'),
            fit: BoxFit.contain,
          ),
          backgroundColor: Color.fromRGBO(30, 128, 129, 0.0),
        ),
        SizedBox(width: double.infinity, height: 20.0,),
      ],
    );

    final bloc = Provider.of(context);

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              child: logo,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 15.0),
            padding: EdgeInsets.symmetric(vertical: 15.0),
            child: Column(
              children: <Widget>[
                Text('Iniciar Sesión', style: TextStyle(fontSize: 25.0, color: Colors.white)),
                SizedBox(height: 60.0),
                _crearEmail(bloc),
                SizedBox(height: 30.0),
                _crearPassword(bloc),
                SizedBox(height: 30.0),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Text('¿Olvidó la contraseña?', textAlign: TextAlign.right, style: TextStyle(color: Colors.white)),
                ),
                SizedBox(height: 30.0),
                _crearBoton(bloc),
                
              ],
            ),
          ),
          Divider(color: Colors.white),
          _botonRegistro(context),
          SizedBox(height: 100.0)
        ],
      ),
    );
  }

  Widget _crearEmail(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.emailStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            cursorColor: Colors.white,
            decoration: InputDecoration(
              icon: Icon(Icons.alternate_email, color: Colors.white),
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
              hintText: 'ejemplo@correo.com',
              hintStyle: TextStyle(color: Colors.white),
              labelText: 'Correo Electrónico',
              labelStyle: TextStyle(color: Colors.white),
              errorText: snapshot.error
            ),
            style: TextStyle(fontFamily: DefaultTextStyle.of(context).style.fontFamily, color: Colors.white),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );
  }

  Widget _crearPassword(LoginBloc bloc){

    return StreamBuilder(
      stream: bloc.passwordStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: true,
            cursorColor: Colors.white,
            decoration: InputDecoration(
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
              icon: Icon(Icons.lock_outline, color: Colors.white),
              labelText: 'Contraseña',
              labelStyle: TextStyle(color: Colors.white),
              errorText: snapshot.error
            ),
            style: TextStyle(fontFamily: DefaultTextStyle.of(context).style.fontFamily, color: Colors.white),
            onChanged: bloc.changePassword,
          ),
        );
      },
    );
  }

  Widget _crearBoton(LoginBloc bloc){
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
            child: Text('Ingresar'),            
          ),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          elevation: 10.0,
          color: Color.fromRGBO(30, 128, 129, 1.0),
          textColor: Colors.white,
          onPressed: snapshot.hasData ? () => _login(bloc, context) : null,
        );
      },
    );
  }

  _login( LoginBloc bloc, BuildContext context) async {
    final _prefs = new PreferenciasUsuario();
    final resp = await http.get( returnMethodServer('Login').toString().replaceAll("{usuario}", bloc.email));
    if(resp.body.isNotEmpty){
      final decodedData = json.decode(resp.body);
      // Validamos la contraseña
      final contraMD5 = md5.convert(utf8.encode(bloc.password)).toString();
      if(decodedData[0]["password"] != contraMD5){
        retornarMensaje(context, "Las contraseñas ingresadas no coinciden.", Icon(Icons.warning, color: Colors.yellow, size: 25.0));
        return;
      }
      _prefs.sessionIniciada = true;
      _prefs.idUsuario = decodedData[0]["idUsuario"];
      Navigator.pushReplacementNamed(context, '/');
    }else{
      retornarMensaje(context, "No existe un registro con el correo especificado.", Icon(Icons.error, color: Colors.red, size: 25.0));
    }
  }

  Widget _botonRegistro(BuildContext context){
    return RaisedButton(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 15.0),
        child: Text('Registrarse'),            
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 10.0,
      color: Color.fromRGBO(30, 128, 129, 1.0),
      textColor: Colors.white,
      onPressed: () => Navigator.pushNamed(context, 'register'),
    );
  }
}