import 'dart:io';

import 'package:flutter/material.dart';
import 'package:tellus/src/utils/preferences_user.dart';
import 'package:tellus/src/utils/url_backend.dart';
import 'package:video_player/video_player.dart';
import 'package:http/http.dart' as http;


class VideoConfigPage extends StatefulWidget {
  _VideoConfigPageState createState() => _VideoConfigPageState();
}

class _VideoConfigPageState extends State<VideoConfigPage> {  
  VoidCallback videoPlayerListener;
  VideoPlayerController videoController;
  VideoPlayerController vcontroller;
  bool reproduciendo = false;
  final _keyForm = GlobalKey<FormState>();
  TextEditingController _tituloController = new TextEditingController();
  TextEditingController _descripcionController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    final String urlVideo = ModalRoute.of(context).settings.arguments;
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Configuración del vídeo'),
        actions: <Widget>[
          FlatButton(
            child: Text("Publicar", style: TextStyle(color: Colors.white)),
            onPressed: () async {
              if(!_keyForm.currentState.validate()){
                return;
              }
              final _prefs = new PreferenciasUsuario();
              // Enviamos datos
              print("Entró");
              var request = new http.MultipartRequest("POST", Uri.parse(returnMethodServer('SubirVideos')));
              request.fields['idUsuario'] = _prefs.idUsuario.toString();
              request.fields['titulo'] = _tituloController.text;
              request.fields['descripcion'] = _descripcionController.text;
              request.files.add(await http.MultipartFile.fromPath(
                  'file',
                  urlVideo
              ));
              request.send().then((response) {
                if (response.statusCode == 200) Navigator.of(context).pushReplacementNamed('/');
              });
            },
          )
        ],
      ),
      body: Form(
        key: _keyForm,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          margin: EdgeInsets.symmetric(horizontal: 15.0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _videoPrev(context, urlVideo),
                _campoTitulo(),
                SizedBox(height: 20.0),
                _campoDescripcion(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _videoPrev(BuildContext context, String url){
    return  videoController == null
            ? GestureDetector(
              child: Container(
                width: double.infinity,
                height: 300.0,
                color: Colors.black,
                child: Center(
                  child: Text('De clic aquí para reproducir el vídeo', style: TextStyle(color: Colors.white)),
                ),
              ),
              onTap: () async{
                await _startVideoPlayer(url);
              },
            )
            : SizedBox(
              child: Stack(
                children: <Widget>[
                  Container(
                    child: Center(
                      child: AspectRatio(
                          aspectRatio:
                              videoController.value.size != null
                                  ? videoController.value.aspectRatio
                                  : 1.0,
                          child: VideoPlayer(videoController)),
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.red),
                      color: Colors.black
                    ),
                  ),
                  IconButton(
                    icon: reproduciendo ? 
                            Icon(Icons.stop, color: Colors.red, size: 40.0) :
                            Icon(Icons.play_arrow, color: Colors.green, size: 40.0) ,
                    onPressed: reproduciendo ? 
                                _stopVideoPlayer : 
                                _playVideoPlayer,
                  )
                ],
              ),
              width: double.infinity,
              height: 300.0,
    );
  }

  Widget _campoTitulo(){
    return TextFormField(
      controller: _tituloController,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        icon: Icon(Icons.textsms, color: Colors.black),
          border: UnderlineInputBorder(),
          hintText: 'Ingrese el título del vídeo',
          labelText: 'Título',
      ),
      validator: (value){
        if(value.length <= 0 || value.length > 100){
          return "El título debe contener entre 1 y 100 carácteres";
        }
        return null;
      },
    );
  }

  Future<void> _startVideoPlayer(String videoPath) async {
    vcontroller = VideoPlayerController.file(File(videoPath));
    videoPlayerListener = () {
      if (videoController != null && videoController.value.size != null) {
        // Refreshing the state to update video player with the correct ratio.
        if (mounted) setState(() {});
        videoController.removeListener(videoPlayerListener);
      }
    };
    vcontroller.addListener(videoPlayerListener);
    await vcontroller.setLooping(false);
    await vcontroller.setVolume(1.0);
    await vcontroller.initialize();
    await videoController?.dispose();
    if (mounted) {
      setState(() {
        videoController = vcontroller;
      });
    }
    reproduciendo = true;
    await vcontroller.play();
  }

  Future<void> _stopVideoPlayer() async {
    await videoController.pause();
    setState(() {
      reproduciendo = false;
    });
  }

  Future<void> _playVideoPlayer() async {
    await videoController.play();
    setState(() {
      reproduciendo = true;
    });
  }

  Widget _campoDescripcion(){
    return TextFormField(
      controller: _descripcionController,
      textCapitalization: TextCapitalization.sentences,      
      decoration: InputDecoration(
        icon: Icon(Icons.textsms, color: Colors.black),
          border: UnderlineInputBorder(),
          hintText: 'Ingrese una breve descripción',
          labelText: 'Descripción',
      ),
      validator: (value) {
        if(value.length <= 0 || value.length >= 250){
          return "La descripción debe contener entre 1 y 250 carácteres";
        }
        return null;
      },
      minLines: 1,
      maxLines: 3,
    );
  }
}