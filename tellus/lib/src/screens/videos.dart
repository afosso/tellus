import 'package:flutter/material.dart';
import 'package:tellus/src/models/noticia_model.dart';
import 'package:tellus/src/providers/historia_provider.dart';
import 'package:tellus/src/providers/noticia_provider.dart';
import 'package:tellus/src/widgets/historia_widget.dart';
import 'package:tellus/src/widgets/video_widget.dart';

class Videos extends StatelessWidget {
  final historiaProvider = new HistoriaProvider();
  final noticiaProvider = new NoticiaProvider();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[250],
      child: _crearListado()
    );
  }

  Widget _crearListado(){
    return FutureBuilder(
      future: noticiaProvider.getNoticias(),
      builder: (BuildContext context, AsyncSnapshot<List<Noticia>> snapshot) {
        if(snapshot.hasData){
          final productos = snapshot.data;

          return ListView.builder(
            itemCount: productos.length,
            itemBuilder: (context, i) => VideoWidget(productos[i]),
          );
        }else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _fotter(context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20.0, top: 10.0),
            child: Text(
              'Historias',
              style: Theme.of(context).textTheme.subhead,
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          FutureBuilder(
            future: historiaProvider.getHistorias(),
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if (snapshot.hasData) {
                return MovieHorizontal(
                  historias: snapshot.data
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          )
        ],
      ),
    );
  }
}