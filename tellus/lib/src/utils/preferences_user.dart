import 'package:shared_preferences/shared_preferences.dart';


class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
    _prefs.setBool('sessionIniciada', false);
  }

  get token {
    return _prefs.getString('token') ?? "";
  }

  set token(String value) {
    _prefs.setString('token', value);
  }

  get sessionIniciada {
    return _prefs.getBool('sessionIniciada') ?? false;
  }

  set sessionIniciada(bool value) {
    _prefs.setBool('sessionIniciada', value);
  }

  get idUsuario {
    return _prefs.getInt('idUsuario') ?? "";
  }

  set idUsuario(int value){
    _prefs.setInt('idUsuario', value);
  }

  
}