import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

obtenerTerminosyCondiciones(BuildContext context, String fontFamily){
  return Container(
    width: MediaQuery.of(context).size.width,
    height: MediaQuery.of(context).size.height * 0.7,
    child: SingleChildScrollView(
      child: Column(
        
        children: <Widget>[
          _montarTitulo('asdasd', fontFamily),
          SizedBox(height: 5),
          _montarDescripcion('Nuestro objetivo es abrir un espacio a las personas para que comuniquen hechos que suceden en las respectivas regiones de Colombia a través de vídeos cortos.', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('Nuestros servicios han sido diseñados para promover el uso de la información de forma veraz y real.', fontFamily),
          SizedBox(height: 5.0),
          _montarTitulo('Introducción', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('Contrato: Cuando utilices nuestros servicios aceptas cumplir todos estos términos. Tu uso de nuestros servicios también está sujetos a nuestra política de cookies y a nuestra política de publicidad, que abarca el modo en que recabamos, utilizamos, compartimos y analizamos tu información personal.', fontFamily),
          SizedBox(height: 5.0),
          _montarTitulo('Estas condiciones se aplican a Miembros y a Visitantes.', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('Si eres visitante o miembro de nuestros servicios, cuales quiera recopilación, uso en formación compartida en relación con tus datos personales quedaran sujetos a esta política de privacidad (que incluye nuestra política de cookies y otros documentos mencionados en esta política de Privacidad) y sus actualizaciones.', fontFamily),
          SizedBox(height: 5.0),
          _montarTitulo('Miembros y Visitantes', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('Al registrarte y unirte al servicio de Parlotea, se te considerará miembro. Si has decidido no registrarte en nuestros servicios, puedes acceder a determinadas funcionalidades como visitante.', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('Para poder acceder algunas funciones del servicio, deberá crear una cuenta de Parlotea nunca debe utilizar la cuenta de otra persona sin su consentimiento. Al crear su cuenta deberá proporcionar información precisa y verdadera. Usted es el único responsable por la actividad de su cuenta y deberá mantener la contraseña de su cuenta protegida.', fontFamily),
          SizedBox(height: 5.0),
          _montarTitulo('Cambios', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('PODEMOS REALIZAR MODIFICACIONES EN EL PRESENTE CONTRATO.', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('Podemos modificar este contrato, nuestra política de privacidad y nuestra política de cookies de vez en cuando. Si introducimos algún cambio importante en el contrato te avisaremos a través de nuestros servicios, o por otros medios, para darte la oportunidad de revisar los cambios antes de que se hagan efectivos. Acordamos que las modificaciones no pueden ser retroactivas. Si no estás de acuerdo con cualquiera de los cambios, estás en la libertad de cerrar tu cuenta. Tu uso continuado de nuestros servicios después de publicar o de enviar un aviso sobre los cambios en estos términos significa que estás de acuerdo con los términos actualizados.', fontFamily),
          SizedBox(height: 5.0),
          _montarTitulo('Obligaciones', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('Requisitos para utilizar los servicios', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('Estas son algunas de las cosas a las que te comprometes en este contrato:', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('- Tener la edad mínima establecida.                        ', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('- Los menores de 16 años no pueden usar nuestros servicios.', fontFamily),
          SizedBox(height: 5.0),
          _montarDescripcion('Para usar los servicios aceptas que debes tener la edad mínima sólo tendrá una cuenta de Parlotea.', fontFamily),
          _montarTitulo('Tu cuenta', fontFamily),
          _montarDescripcion('Mantendrás tu contraseña en secreto', fontFamily),
          _montarDescripcion('No compartirás tu cuenta con ninguna otra persona y respetarás nuestras normas y legislación', fontFamily),
          _montarTitulo('Avisos y mensajes', fontFamily)
        ],
      ),
    ),
  );
}

_montarTitulo(String titulo, String fontFamily) {
  return Text(titulo, style: TextStyle(fontFamily: 'Roboto', fontWeight: FontWeight.bold), textAlign: TextAlign.center);
}

_montarDescripcion(String descripcion, String fontFamily) {
  return Text(descripcion,
              style: TextStyle(fontSize: 12.0, fontFamily: fontFamily), textAlign: TextAlign.justify);
}