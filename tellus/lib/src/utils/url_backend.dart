final _urlServer = "http://www.tellus.com.co/BackendTellus/public/api";

  returnMethodServer(String option){
  switch (option) {
    case "RegistrarPersona":
      return _urlServer + "/Persona/NewPerson";
    case "Login":
      return _urlServer + "/Usuario/{usuario}";
    case "SubirVideos":
      return _urlServer + "/SubirVideo";
    case "Noticias":
      return _urlServer + "/Noticias";
    default:
  }
}