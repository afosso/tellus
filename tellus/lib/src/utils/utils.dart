import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

retornarMensaje(BuildContext context, String mensaje, Icon icono){
  showDialog(
    context: context,
    builder: (context) { 
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        content: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              icono,
              Text(mensaje)
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: (){
              Navigator.pop(context);
            },
            child: Text('Aceptar', style: TextStyle(color: Theme.of(context).primaryColor),),
          )
        ],
      );
    }
  );
}

retornarSnackBar(BuildContext context, String texto){
  final snackBar = SnackBar(content: Text(texto));
  Scaffold.of(context).showSnackBar(snackBar);
}