import 'package:flutter/material.dart';
import 'package:tellus/src/models/historia_model.dart';

class MovieHorizontal extends StatelessWidget {
  final List<Historia> historias;

  final _pageController = new PageController(initialPage: 1, viewportFraction: 0.3);

  MovieHorizontal({@required this.historias});

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      height: _screenSize.height * 0.25,
      child: PageView.builder(
        pageSnapping: false,
        controller: _pageController,
        //children: _tarjetas(context),
        itemCount: historias.length,
        itemBuilder: (context, index) {
          return _tarjeta(context, historias[index]);
        },
      ),
    );
  }

  Widget _tarjeta(BuildContext context, Historia historia) {
    historia.id = '${historia.id}-poster';

    final tarjeta = Container(
      margin: EdgeInsets.only(right: 5.0),
      child: Column(
        children: <Widget>[
          Hero(
            tag: historia.id,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: FadeInImage(
                image: AssetImage(historia.ruta),
                placeholder: AssetImage('assets/loading.gif'),
                fit: BoxFit.cover,
                height: MediaQuery.of(context).size.height * 0.2,
              ),
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Text(historia.nombre,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.caption)
        ],
      ),
    );

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, 'detalle', arguments: historia);
      },
      child: tarjeta,
    );
  }

  List<Widget> _tarjetas(context) {
    return historias.map((pelicula) {
      return Container(
        margin: EdgeInsets.only(right: 15.0),
        child: Column(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: FadeInImage(
                image: NetworkImage(pelicula.ruta),
                placeholder: AssetImage('assets/loading.gif'),
                fit: BoxFit.cover,
                height: MediaQuery.of(context).size.height * 0.2,
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            Text(pelicula.nombre,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.caption)
          ],
        ),
      );
    }).toList();
  }
}
