import 'package:flutter/material.dart';
import 'package:tellus/src/models/noticia_model.dart';

class VideoWidget extends StatelessWidget {

  final Noticia _noticia;

  VideoWidget(this._noticia);


  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      child: Card(
        elevation: 5.0,
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: ListTile(
                leading: CircleAvatar(
                  backgroundImage: NetworkImage('http://i.pravatar.cc/300'),
                  radius: 20.0,
                ),
                title: Text('${_noticia.nombres} ${_noticia.apellidos}'),
                subtitle: Text(_noticia.fechaPublicacion),
              ),
            ),
            ListTile(
              title: Text(_noticia.tituloVideo),
              subtitle: Text(_noticia.descripcion),
            ),
            Container(
              width: double.infinity,
              height: size.height * 0.35,
              color: Colors.black,
              child: Center(
                child: Text('Previsualización del vídeo', style: TextStyle(color: Colors.white)),
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.thumb_up),
                    onPressed: (){},
                  ),
                  IconButton(
                    icon: Icon(Icons.thumb_down),
                    onPressed: (){},
                  ),
                  IconButton(
                    icon: Icon(Icons.share),
                    onPressed: (){},
                  ),
                  IconButton(
                    icon: Icon(Icons.star),
                    onPressed: (){},
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}